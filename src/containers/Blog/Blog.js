import React, { Component } from 'react';

// Jika pakai Configure Custom
// import axios from '../../axios'
// Jika pakai configure Default dari index.js
import axios from 'axios'
import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';
import './Blog.css';

// Did Mount : Potong dari 0 dan ambil 4 (termasuk yang 0 tadi), map nya itu kita transform dan tambahkan 1 object lagi setelah object response dengan author, dia jadi gini ntar :
// {id : 1, title: 'blabla', author: 'julian'}
// Catch, jika ada error maka tampilkan Pesan
class Blog extends Component {
    state = {
        posts: [],
        selectedPostId: null,
        error: false
    }

    componentDidMount() {
        axios.get('/posts')
            .then(response => {
                const posts = response.data.slice(0,4);
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                        author: 'Joel'
                    }
                })
                this.setState({posts : updatedPosts})
            })
            .catch(error => {
                this.setState({error: true})
            })
    }

    postSelectedHandler = (id) => {
        this.setState({selectedPostId : id})
    }

    postAddedState = (id, title, body, author) => {
        const newState = {
            id: id,
            title: title,
            body: body,
            author: author
        }

        const newStateManage = [
            ...this.state.posts,
            newState
        ];

        this.setState({posts: newStateManage})
    }

    deleteSelectedState = (id) => {
        console.log(id);
        const posts = [...this.state.posts];
        const findIndexPost = posts.findIndex(post => {
           return post.id === id
        });
        posts.splice(findIndexPost, 1);
        this.setState({posts: posts});
    }

    render () {
        let posts = <p style={{textAlign:'center'}}>Something Went Wrong!</p>
        if (!this.state.error) {
            posts = this.state.posts.map((data) => {
                return <Post 
                            key={data.id} 
                            title={data.title} 
                            author={data.author}
                            clicked={() => this.postSelectedHandler(data.id)}
                        />
            })
        }

        console.log(this.state.posts)

        return (
            <div>
                <section className="Posts">
                   {posts}
                </section>
                <section>
                    <FullPost id={this.state.selectedPostId} stateDeleted={this.deleteSelectedState}/>
                </section>
                <section>
                    <NewPost stateAdded={this.postAddedState}/>
                </section>
            </div>
        );
    }
}

export default Blog;