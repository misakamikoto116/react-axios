import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios'

// Global Configuration
// interceptor :
// jika ada error Maka akan Reject.. lalu munculkan error, jadi setiap ada axios yang ter catch error maka akan muncul suatu console.log
// Di Bawah use itu, jika dia di load maka akan muncul console.log dari request / response juga

// Axios Base url : Memberikan Base Url / Seperti ENV ketika Ingin melakukan http request dengan axios
// header Commond authorization : hanya menerima Auth Token
// Header Post Contenten-type : Hanya menerima Application/json untuk axios post
axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN';
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.request.use(request => {
    console.log(request);
    // Edit request config
    return request;
}, error => {
    console.log(error)
    return Promise.reject(error)
})

axios.interceptors.response.use(response => {
    console.log(response);
    // Edit response config
    return response;
}, error => {
    console.log(error)
    return Promise.reject(error)
})

ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
