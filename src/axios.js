// File ini gunanya buat component yang menggunakan url atau configure custom.. misal : default nya jsonplaceholder, trus kita mau ke yang lain.. nah bisa pakai yang ini.. jadi nnti di componentnya tidak menggunakan import axios from 'axios' lagi tapi: 
// import axios from '{Lokasi file ini}'
import axios from 'axios'

// Copy Axios. dan membuat yang baru
const instance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com'
})

axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

//intance.interceptor.request.. apapun itu

export default instance